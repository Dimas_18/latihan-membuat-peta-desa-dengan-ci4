<?= $this->extend('Template'); ?>
<?= $this->section('content'); ?>
<div class="container-fluid">
    <div id="maps" style="width: 100%; height: 90vh;"></div>
</div>
<script>
    var defaultmap = L.tileLayer('https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        attribution: '© Google Maps',
        maxZoom: 20,
    });

    var cartodb = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
        maxZoom: 20,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://carto.com/attributions">CARTO</a>'
    });

    var street = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Map data &copy; <a href="https://www.arcgis.com/">ArcGIS</a>'
    });

    var satellite = L.tileLayer('https://{s}.google.com/vt/lyrs=y&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
    });

    var ground = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
        attribution: 'Map data &copy; <a href="https://www.google.com/maps">Google Maps</a>'
    });

    var maps = L.map('maps', {
        center: [-7.388889078548703, 109.96373235416648],
        zoom: 15,
        layers: [defaultmap]
    });

    const baseLayers = {
        'Default': defaultmap,
        'CartoDB': cartodb,
        'Jalan' : street,
        'Satelit' : satellite,
        'Ketinggian' : ground
    };

    const layerControl = L.control.layers(baseLayers, null, {
        collapsed: true
    }).addTo(maps);

    var rt = {'weight': 3}
    var jalan = {
        'color': 'red',
        'dashArray': '7',
        'weight': 6
    }
    var vb = {
        'color': 'gray',
        'opacity': 1,
        'weight': 2,
        'fillColor': 'white',
        'fillOpacity': 0.5
    }

    L.geoJSON({
        "type": "FeatureCollection",
        "features": [<?= $vb; ?>]
    }, {
        style: vb
    }).addTo(maps);

    <?php foreach ($rukun_tetangga as $rukun_tetangga => $rt) { ?>
    L.geoJSON({
        "type": "FeatureCollection",
        "features": [<?= $rt->koordinat; ?>]
    }, {
        style: rt
    }).addTo(maps).on('click', function() {
        Swal.fire({
            title: '<span class="text-uppercase"><?= $rt->nama; ?></span>',
            html: '<p class="my-1"><?= $rt->jumlah; ?> Jiwa</p><p class="small my-1"><?= $rt->alamat; ?></p>',
            imageUrl: '<?= $rt->gambar; ?>',
            imageHeight: 200,
            imageAlt: '<?= $rt->nama; ?>'
        })
    });
    <?php } ?>

    <?php foreach ($jalanan as $jalanan => $jalan) { ?>
    L.geoJSON({
        "type": "FeatureCollection",
        "features": [<?= $jalan->koordinat; ?>]
    }, {
        style: jalan
    }).addTo(maps).on('click', function() {
        Swal.fire({
            title: '<span class="text-capitalize"><?= $jalan->nama; ?></span>'
        })
    });
    <?php } ?>

    <?php foreach ($fasilitas_umum as $fasilitas_umum => $fasilitas) { ?>
    L.marker([<?= $fasilitas->koordinat; ?>]).addTo(maps).on('click', function() {
        Swal.fire({
            title: '<span class="text-capitalize"><?= $fasilitas->nama; ?></span>',
            html: '<a href="/cari?keyword=<?= $fasilitas->jenis; ?>" class="text-capitalize h5 text-primary"><?= $fasilitas->jenis; ?></a><p class="small my-1"><?= $fasilitas->alamat; ?></p><p class="small my-1"><?= $fasilitas->no_telp; ?></p>',
            imageUrl: '<?= $fasilitas->gambar; ?>',
            imageHeight: 200,
            imageAlt: '<?= $fasilitas->nama; ?>'
        })
    });
    <?php } ?>
</script>
<?= $this->endSection(); ?>