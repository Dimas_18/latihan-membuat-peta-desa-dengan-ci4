<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

$routes->setAutoRoute(true);
$routes->get('/', 'Home::index');
$routes->get('/batas_rt', 'Home::rt');
$routes->get('/jalanan', 'Home::jalan');
$routes->get('/fasum', 'Home::fasilitas');
$routes->get('/cari', 'Home::cari'); // Hanya berfungsi dengan menjalankan 'php spark serve' di terminal