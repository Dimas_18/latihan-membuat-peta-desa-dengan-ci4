<?php

namespace App\Controllers;

use App\Models\M_batas;
use App\Models\M_jalan;
use App\Models\M_fasilitas;

class Home extends BaseController
{
    protected $M_batas;
    protected $M_jalan;
    public $M_fasilitas;

    public function __construct()
    {
        $this->M_batas = new M_batas();
        $this->M_jalan = new M_jalan();
        $this->M_fasilitas = new M_fasilitas();
    }
    
    public function index()
    {
        $data = [
            'title' => 'Home | Peta Desa',
            'vb' => $this->vb,
            'rukun_tetangga' => $this->M_batas->findAll(),
            'jalanan' => $this->M_jalan->findAll(),
            'fasilitas_umum' => $this->M_fasilitas->findAll(),
            'getsegment1' => $this->request->uri->getSegment(1)
        ];
        return view('index', $data);
    }

    public function rt() {
        $data = [
            'title' => 'Home | Peta Desa',
            'vb' => $this->vb,
            'rukun_tetangga' => $this->M_batas->findAll(),
            'getsegment1' => $this->request->uri->getSegment(1)
        ];
        return view('rt', $data);
    }

    public function jalan()
    {
        $data = [
            'title' => 'Home | Peta Desa',
            'vb' => $this->vb,
            'jalanan' => $this->M_jalan->findAll(),
            'getsegment1' => $this->request->uri->getSegment(1)
        ];
        return view('jalan', $data);
    }

    public function fasilitas()
    {
        $data = [
            'title' => 'Home | Peta Desa',
            'vb' => $this->vb,
            'fasilitas_umum' => $this->M_fasilitas->findAll(),
            'getsegment1' => $this->request->uri->getSegment(1)
        ];
        return view('fasilitas', $data);
    }

    public function cari() // Hanya berfungsi dengan menjalankan 'php spark serve' di terminal
    {
        $keyword = $this->request->getVar('keyword');
        // dd($keyword);
        $data = [
            'title' => 'Home | Peta Desa',
            'keyword' => $keyword,
            'vb' => $this->vb,
            'rukun_tetangga' => $this->M_batas->search($keyword)->findAll(),
            'jalanan' => $this->M_jalan->search($keyword)->findAll(),
            'fasilitas_umum' => $this->M_fasilitas->search($keyword)->findAll(),
            'getsegment1' => $this->request->uri->getSegment(1)
        ];
        return view('cari', $data);
    }
}
